import React from 'react';
import {Helmet} from "react-helmet";
import SearchBar from './SearchBar';
import youtube from '../api/youtube';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';

class App extends React.Component{
    state = { videos: [] , selectedVideo:null};

    componentDidMount() {
        this.onSearchSubmit('smart city jakarta');
    }   

    onSearchSubmit = async (term) => {
        const response = await youtube.get('/search',{
            params: {
                q: term
            } 
        });
        this.setState({
            videos: response.data.items,
            selectedVideo: response.data.items[0]
        });
    };

    onVideoSelect = (video) => {
        this.setState({selectedVideo: video});
    }

    render() {
        return (
            <div className="application">
            <Helmet>
                <meta charSet="utf-8" />
                <title>My Title</title>
                <link rel="canonical" href="http://example.com/example" />
                <meta name="linkaja_partner" content="LinkAja"/>
                <meta name="linkaja_title" content="Hooy aku title"/>
                <meta name="linkaja_description" content=""/>
            </Helmet>
            <div className="ui container">
                <SearchBar onFormSubmit={this.onSearchSubmit} />
                {this.state.videos.length} Videos.
                <div className="ui grid">
                    <div className="ui row">
                        <div className="eleven wide column">
                            <VideoDetail video={this.state.selectedVideo} />
                        </div>
                        <div className="five wide column">
                            <VideoList 
                                onVideoSelect={this.onVideoSelect} 
                                videos={this.state.videos} />
                        </div>
                    </div>
                </div>
            </div>
            </div>
        );
    }
}

export default App;
